System Monitor Viewer
=====================

A PHP website to view the aggregated data from a running system monitor node.

See Also
--------

* [System Monitor Node](https://gitlab.com/aimnadze/system-monitor-node)
* [System Monitor SMSGlobal Notifier](https://gitlab.com/aimnadze/system-monitor-smsglobal-notifier)
