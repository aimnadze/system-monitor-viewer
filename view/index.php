<?php

function create_total_storage ($totalStorage) {
    if ($totalStorage) return format_bytes($totalStorage);
    return '<b>Unknown</b>';
}

function create_filesystems ($fileSystems) {
    if (count($fileSystems) <= 1) return;
    $html = '<div id="fileSystems">';
    foreach ($fileSystems as $fileSystem) {
        $total = $fileSystem->total * 1024;
        $html .=
            '<div style="padding: 2px">'
                .'Used '
                .format_used_bytes($fileSystem->used * 1024, $total)
                .' out of total '
                .format_bytes($total)
                .' on '
                .'<b>'.htmlspecialchars($fileSystem->mountPoint).'</b>'
                .'.'
            .'</div>'
            .'<div class="graph"></div>';
    }
    $html .= '</div>';
    return $html;
}

function create_used_storage ($usedStorage, $totalStorage) {
    if ($totalStorage) return format_used_bytes($usedStorage, $totalStorage);
    return '<b>Unknown</b>';
}

function format_bytes ($bytes) {
    return
        '<b title="'.number_format($bytes).' bytes">'
            .bytestr($bytes)
        .'</b>';
}

function format_used_bytes ($usedBytes, $totalBytes) {
    return
        '<b title="'.number_format($usedBytes).' bytes">'
            .bytestr($usedBytes).' ('.floor($usedBytes * 100 / $totalBytes).'%)'
        .'</b>';
}

include_once '../fns/bytestr.php';
include_once '../fns/request_strings.php';
include_once '../lib/debug.php';
include_once '../lib/defaults.php';

list($address, $hostHeader, $key, $duration) = request_strings(
    'address', 'hostHeader', 'key', 'duration');

if ($address === '') $addressValue = $defaults['address'];
else $addressValue = $address;

if ($hostHeader === '') $hostHeaderValue = $defaults['hostHeader'];
else $hostHeaderValue = $hostHeader;

if ($duration === '1h') {
    $timeScale = 60;
    $refresh = 60;
    $propertySuffix = '';
} elseif ($duration === '24h') {
    $timeScale = 60 * 24;
    $refresh = 60 * 20;
    $propertySuffix = '20';
} elseif ($duration === '7d') {
    $timeScale = 60 * 24 * 7;
    $refresh = 60 * 60 * 2;
    $propertySuffix = '120';
} elseif ($duration === '30d') {
    $timeScale = 60 * 24 * 30;
    $refresh = 60 * 60 * 10;
    $propertySuffix = '600';
} else {
    $duration = '360d';
    $timeScale = 60 * 24 * 360;
    $refresh = 60 * 60 * 24 * 5;
    $propertySuffix = '7200';
}

$quitHref = '../?'.htmlspecialchars(
    http_build_query([
        'address' => $address,
        'hostHeader' => $hostHeader,
        'key' => $key,
    ])
);

$reloadHref = '?'.htmlspecialchars(
    http_build_query([
        'address' => $address,
        'hostHeader' => $hostHeader,
        'key' => $key,
    ])
);

$title = htmlspecialchars($addressValue).' status';

$ch = curl_init();
curl_setopt_array($ch, [
    CURLOPT_URL => "http://$addressValue/$key",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_TIMEOUT => 10,
    CURLOPT_HTTPHEADER => ["Host: $hostHeaderValue"],
]);
$response = curl_exec($ch);
if ($response) {
    $data = json_decode($response);
    if ($data) {

        $uptime = floor((double)$data->uptime);

        include_once 'fns/format_uptime.php';

        $memory = $data->memory;
        $totalMemory = $memory->total;
        $usedMemory = $memory->used;

        $loadavg = $data->loadavg;
        $loadavg = number_format($loadavg[0], 2).', '.number_format($loadavg[1], 2).', '.number_format($loadavg[2], 2);

        $storage = $data->storage;
        $totalStorage = $storage->total * 1024;
        $usedStorage = $storage->used * 1024;

        $memoryValues = [];
        $property = "memoryHistory$propertySuffix";
        foreach ($data->$property as $item) {
            $memoryValues[] = [
                'time' => $item->time,
                'value' => $memory->total - $item->value,
            ];
        }

        $processorValues = [];
        $property = "loadavgHistory$propertySuffix";
        foreach ($data->$property as $item) {
            $processorValues[] = [
                'time' => $item->time,
                'value' => $item->value[0] / $data->numCpus,
            ];
        }

        $networkValues = [];
        $property = "networkHistory$propertySuffix";
        foreach ($data->$property as $item) {
            $networkValues[] = [
                'time' => $item->time,
                'value' => $item->sent + $item->received,
            ];
        }

        $temperatureValues = [];
        $property = "temperatureHistory$propertySuffix";
        if (property_exists($data, $property)) {
            foreach ($data->$property as $item) {
                $cpu = $item->cpu;
                $mb = $item->mb;
                if ($cpu === null && $mb === null) continue;
                $temperatureValues[] = [
                    'time' => $item->time,
                    'value' => max($item->cpu, $item->mb),
                ];
            }
        }

        $storageValues = [];
        $property = "storageHistory$propertySuffix";
        foreach ($data->$property as $item) {
            $storageValues[] = [
                'time' => $item->time,
                'value' => $item->used,
            ];
        }

        $fileSystems = [];
        foreach ($data->storage->fileSystems as $item) {

            $mountPoint = $item->mountPoint;
    
            $values = [];
            foreach ($data->$property as $item) {
                foreach ($item->fileSystems as $fileSystem) {
                    if ($fileSystem->mountPoint == $mountPoint) {
                        $values[] = [
                            'time' => $item->time,
                            'value' => $fileSystem->used,
                        ];
                    }
                }
            }

            $fileSystems[] = [
                'mountPoint' => $mountPoint,
                'values' => $values,
            ];

        }

        $clientData = [
            'memory' => $memoryValues,
            'processor' => $processorValues,
            'network' => $networkValues,
            'storage' => [
                'values' => $storageValues,
                'fileSystems' => $fileSystems,
            ],
            'temperature' => $temperatureValues,
        ];

        $uptimeTotal = $data->uptimeTotal;

        $content =
            '<div>'
                ."<a href=\"$reloadHref&amp;duration=1h\" class=\"button".($duration === '1h' ? ' active' : '')."\" style=\"margin-left: 8px\">"
                    .'1 hour'
                .'</a>'
                ."<a href=\"$reloadHref&amp;duration=24h\" class=\"button".($duration === '24h' ? ' active' : '')."\" style=\"margin-left: 8px\">"
                    .'24 hours'
                .'</a>'
                ."<a href=\"$reloadHref&amp;duration=7d\" class=\"button".($duration === '7d' ? ' active' : '')."\" style=\"margin-left: 8px\">"
                    .'7 days'
                .'</a>'
                ."<a href=\"$reloadHref&amp;duration=30d\" class=\"button".($duration === '30d' ? ' active' : '')."\" style=\"margin-left: 8px\">"
                    .'30 days'
                .'</a>'
                ."<a href=\"$reloadHref&amp;duration=360d\" class=\"button".($duration === '360d' ? ' active' : '')."\" style=\"margin-left: 8px\">"
                    .'360 days'
                .'</a>'
            .'</div>'
            .'<div style="text-align: left; width: 904px">'
                .'<div style="display: inline-block; vertical-align: top; width: 50%">'
                    .'<div class="item">'
                        .'<div class="label">'
                            .'<label>General</label>'
                        .'</div>'
                        .'<div class="field">'
                            .'<div class="property">'
                                .'<div>Uptime:</div>'
                                .'<div title="'.number_format($uptime).' seconds">'
                                    .'<div>'
                                        .'Last '.format_uptime($uptime)
                                    .'</div>'
                                    .'<div>'
                                        .'Total '.number_format($uptimeTotal * 100 / ($uptimeTotal + $data->downtimeTotal), 2).'%'
                                    .'</div>'
                                .'</div>'
                            .'</div>'
                            .'<div class="property">'
                                .'<div>Load average:</div>'
                                ."<div>$loadavg</div>"
                            .'</div>'
                        .'</div>'
                    .'</div>'
                    .'<div class="item">'
                        .'<div class="label">'
                            .'<label>Storage</label>'
                        .'</div>'
                        .'<div class="field">'
                            .'<div style="padding: 0 2px 2px 2px">'
                                .'Used '
                                .create_used_storage($usedStorage, $totalStorage)
                                .' out of total '
                                .create_total_storage($totalStorage)
                                .'.'
                            .'</div>'
                            .'<div class="graph" id="storageGraphWrapper"></div>'
                            .create_filesystems($storage->fileSystems)
                        .'</div>'
                    .'</div>'
                .'</div>'
                .'<div style="display: inline-block; vertical-align: top; width: 50%">'
                    .'<div class="item">'
                        .'<div class="label">'
                            .'<label>Memory</label>'
                        .'</div>'
                        .'<div class="field">'
                            .'<div style="padding: 0 2px 2px 2px">'
                                .'Used '
                                .format_used_bytes($usedMemory, $totalMemory)
                                .' out of total '
                                .format_bytes($totalMemory)
                                .'.'
                            .'</div>'
                            .'<div class="graph" id="memoryGraphWrapper"></div>'
                        .'</div>'
                    .'</div>'
                    .'<div class="item">'
                        .'<div class="label">'
                            .'<label>Processor</label>'
                        .'</div>'
                        .'<div class="field">'
                            .'<div class="graph" id="processorGraphWrapper"></div>'
                        .'</div>'
                    .'</div>'
                    .'<div class="item">'
                        .'<div class="label">'
                            .'<label>Network</label>'
                        .'</div>'
                        .'<div class="field">'
                            .'<div class="graph" id="networkGraphWrapper"></div>'
                        .'</div>'
                    .'</div>'
                    .'<div class="item">'
                        .'<div class="label">'
                            .'<label>Temperature</label>'
                        .'</div>'
                        .'<div class="field">'
                            .'<div class="graph" id="temperatureGraphWrapper"></div>'
                        .'</div>'
                    .'</div>'
                .'</div>'
            .'</div>'
            .'<script type="text/javascript">'
            .'var data = '.json_encode($clientData)."\n"
            ."var timeScale = $timeScale\n"
            .'</script>';

        if (defined('DEBUG')) {
            $content .=
                '<script type="text/javascript" src="javascript/FormatBytes.js"></script>'
                .'<script type="text/javascript" src="javascript/FormatPercent.js"></script>'
                .'<script type="text/javascript" src="javascript/Graph.js"></script>'
                .'<script type="text/javascript" src="javascript/Legend.js"></script>'
                .'<script type="text/javascript" src="javascript/MemoryGraph.js"></script>'
                .'<script type="text/javascript" src="javascript/NetworkGraph.js"></script>'
                .'<script type="text/javascript" src="javascript/ProcessorGraph.js"></script>'
                .'<script type="text/javascript" src="javascript/StorageGraph.js"></script>'
                .'<script type="text/javascript" src="javascript/TemperatureGraph.js"></script>'
                .'<script type="text/javascript" src="index.js"></script>';
        } else {
            $content .=
                '<script type="text/javascript" src="index.compressed.js?11"></script>';
        }

    } else {
        $content = '<div class="errorText">The key is invalid.</div>';
    }
} else {
    $content =
        '<div class="errorText">'
            .htmlspecialchars(curl_error($ch))
        .'</div>';
}

header('Content-Type: text/html; charset=UTF-8');

if (defined('DEBUG')) {
    $cssLinks =
        '<link rel="stylesheet" type="text/css" href="javascript/Graph.css" />'
        .'<link rel="stylesheet" type="text/css" href="javascript/Legend.css" />'
        .'<link rel="stylesheet" type="text/css" href="../common.css" />'
        .'<link rel="stylesheet" type="text/css" href="index.css" />';
} else {
    $cssLinks = '<link rel="stylesheet" type="text/css" href="index.compressed.css?15" />';
}

echo
    '<!DOCTYPE html>'
    .'<html>'
        .'<head>'
            ."<title>$title</title>"
            .'<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'
            ."<meta http-equiv=\"Refresh\" content=\"$refresh\" />"
            .'<link rel="icon" type="text/html" href="../images/favicon.png" />'
            .'<meta name="viewport" content="width=904; height=594" />'
            .$cssLinks
        .'</head>'
        .'<body>'
            .'<div style="position: absolute; top: 0; right: 0; left: 0">'
                .'<div style="padding: 16px">'
                    .'<div style="position: relative">'
                        ."<h1>$title</h1>"
                        .'<div>Host header: '.htmlspecialchars($hostHeaderValue).'</div>'
                        .'<div style="position: absolute; top: 0; right: 0">'
                            ."<a class=\"button\" href=\"$reloadHref&amp;duration=$duration\">Reload</a>"
                            ."<a class=\"button\" style=\"margin-left: 8px\" href=\"$quitHref\">Quit</a>"
                        .'</div>'
                    .'</div>'
                .'</div>'
            .'</div>'
            .'<div style="position: absolute; top: 80px; right: 0; bottom: 0; left: 0">'
                .'<div style="display: inline-block; vertical-align: middle; height: 100%"></div>'
                .'<div style="display: inline-block; vertical-align: middle">'
                    .$content
                .'</div>'
            .'</div>'
        .'</body>'
    .'</html>';
