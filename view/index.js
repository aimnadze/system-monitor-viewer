(function (data, timeScale) {

    var dateNow = data.dateNow

    ;(function () {
        var memoryGraph = MemoryGraph(dateNow, data.memory, timeScale)
        var wrapper = document.getElementById('memoryGraphWrapper')
        wrapper.appendChild(memoryGraph.element)
    })()

    ;(function () {
        var processorGraph = ProcessorGraph(dateNow, data.processor, timeScale)
        var wrapper = document.getElementById('processorGraphWrapper')
        wrapper.appendChild(processorGraph.element)
    })()

    ;(function () {
        var networkGraph = NetworkGraph(dateNow, data.network, timeScale)
        var wrapper = document.getElementById('networkGraphWrapper')
        wrapper.appendChild(networkGraph.element)
    })()

    ;(function () {
        var temperatureGraph = TemperatureGraph(dateNow, data.temperature, timeScale)
        var wrapper = document.getElementById('temperatureGraphWrapper')
        wrapper.appendChild(temperatureGraph.element)
    })()

    ;(function () {
        var storageGraph = StorageGraph(dateNow, data.storage.values, timeScale)
        var wrapper = document.getElementById('storageGraphWrapper')
        wrapper.appendChild(storageGraph.element)
    })()

    ;(function () {
        var fileSystems = document.getElementById('fileSystems')
        if (fileSystems) {
            var graphs = fileSystems.querySelectorAll('.graph')
            for (var i = 0; i < graphs.length; i++) {
                var values = data.storage.fileSystems[i].values
                var storageGraph = StorageGraph(dateNow, values, timeScale)
                graphs[i].appendChild(storageGraph.element)
            }
        }
    })()

})(data, timeScale)
