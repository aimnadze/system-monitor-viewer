(function () {
function FormatBytes (bytes) {
    var names = ['', 'K', 'M', 'G', 'T']
    for (var i in names) {
        if (bytes > 1024) bytes /= 1024
        else {
            return bytes.toFixed(1) + names[i] + 'B'
        }
    }
}
;
function FormatPercent (n) {
    return (n * 100).toFixed(2) + '%'
}
;
function Graph (lastTime, items, timeScale) {

    var classPrefix = 'Graph'

    items = items.slice()

    var canvas = document.createElement('canvas')
    canvas.className = classPrefix + '-canvas'
    canvas.width = 400
    canvas.height = 100

    var padding = 2,
        graphWidth = canvas.width - padding * 2,
        graphHeight = canvas.height - padding * 2

    var element = document.createElement('div')
    element.className = classPrefix

    if (items.length) {

        function scale (value) {
            if (maxValue == minValue) return -graphHeight / 2
            return -(value - minValue) / (maxValue - minValue) * graphHeight
        }

        var maxValue = -Infinity, minValue = Infinity
        items.forEach(function (item) {
            maxValue = Math.max(maxValue, item.value)
            minValue = Math.min(minValue, item.value)
        })

        var c = canvas.getContext('2d')
        c.lineWidth = 1.4
        c.lineCap = c.lineJoin = 'round'
        c.translate(canvas.width - padding, canvas.height - padding)
        c.beginPath()
        var prevItem = items.pop()
        var prevY = scale(prevItem.value)
        items.reverse()
        c.arc(0, prevY, 1.4, 0, Math.PI * 2)
        c.moveTo(0, prevY)
        items.forEach(function (item) {
            var x = (prevItem.time - item.time) / (1000 * 60) * (graphWidth / timeScale)
            var y = scale(item.value)
            c.translate(-x, 0)
            c.bezierCurveTo(x / 2, prevY, x / 2, y, 0, y)
            prevItem = item
            prevY = y
        })
        c.strokeStyle = '#07f'
        c.stroke()

        element.appendChild(canvas)

    } else {
        var emptyElement = document.createElement('div')
        emptyElement.className = classPrefix + '-empty'
        emptyElement.appendChild(document.createTextNode('No data avaialable'))
        element.appendChild(emptyElement)
    }

    return { element: element }

}
;
function Legend () {

    var element = document.createElement('div')
    element.className = 'Legend'

    return {
        element: element,
        addLine: function (lineElement) {
            var div = document.createElement('div')
            div.appendChild(lineElement)
            element.appendChild(div)
        },
    }

}
;
function MemoryGraph (lastTime, items, timeScale) {

    var graph = Graph(lastTime, items, timeScale)

    if (items.length) {

        var minValue = Infinity,
            maxValue = -Infinity
        items.forEach(function (item) {
            var value = item.value
            if (value > maxValue) maxValue = value
            if (value < minValue) minValue = value
        })

        var legend = Legend()
        legend.addLine(document.createTextNode('Max: ' + FormatBytes(maxValue)))
        legend.addLine(document.createTextNode('Min: ' + FormatBytes(minValue)))
        graph.element.appendChild(legend.element)

    }

    return graph

}
;
function NetworkGraph (lastTime, items, timeScale) {

    var graph = Graph(lastTime, items, timeScale)

    if (items.length) {

        var minValue = Infinity,
            maxValue = -Infinity
        items.forEach(function (item) {
            var value = item.value
            if (value > maxValue) maxValue = value
            if (value < minValue) minValue = value
        })

        var legend = Legend()
        legend.addLine(document.createTextNode('Max: ' + FormatBytes(maxValue) + '/sec'))
        legend.addLine(document.createTextNode('Min: ' + FormatBytes(minValue) + '/sec'))
        graph.element.appendChild(legend.element)

    }

    return graph

}
;
function ProcessorGraph (lastTime, items, timeScale) {

    var graph = Graph(lastTime, items, timeScale)

    if (items.length) {

        var minValue = Infinity,
            maxValue = -Infinity
        items.forEach(function (item) {
            var value = item.value
            if (value > maxValue) maxValue = value
            if (value < minValue) minValue = value
        })

        var legend = Legend()
        legend.addLine(document.createTextNode('Max: ' + FormatPercent(maxValue)))
        legend.addLine(document.createTextNode('Min: ' + FormatPercent(minValue)))
        graph.element.appendChild(legend.element)

    }

    return graph

}
;
function StorageGraph (lastTime, items, timeScale) {

    var graph = Graph(lastTime, items, timeScale)

    if (items.length) {

        var minValue = Infinity,
            maxValue = -Infinity
        items.forEach(function (item) {
            var value = item.value
            if (value > maxValue) maxValue = value
            if (value < minValue) minValue = value
        })

        var legend = Legend()
        legend.addLine(document.createTextNode('Max: ' + FormatBytes(maxValue * 1024)))
        legend.addLine(document.createTextNode('Min: ' + FormatBytes(minValue * 1024)))
        graph.element.appendChild(legend.element)

    }

    return graph

}
;
function TemperatureGraph (lastTime, items, timeScale) {

    var graph = Graph(lastTime, items, timeScale)

    if (items.length) {

        var minValue = Infinity,
            maxValue = -Infinity
        items.forEach(function (item) {
            var value = item.value
            if (value > maxValue) maxValue = value
            if (value < minValue) minValue = value
        })

        var legend = Legend()
        legend.addLine(document.createTextNode('Max: ' + maxValue.toFixed(1) + '\xb0c'))
        legend.addLine(document.createTextNode('Min: ' + minValue.toFixed(1) + '\xb0c'))
        graph.element.appendChild(legend.element)

    }

    return graph

}
;
(function (data, timeScale) {

    var dateNow = data.dateNow

    ;(function () {
        var memoryGraph = MemoryGraph(dateNow, data.memory, timeScale)
        var wrapper = document.getElementById('memoryGraphWrapper')
        wrapper.appendChild(memoryGraph.element)
    })()

    ;(function () {
        var processorGraph = ProcessorGraph(dateNow, data.processor, timeScale)
        var wrapper = document.getElementById('processorGraphWrapper')
        wrapper.appendChild(processorGraph.element)
    })()

    ;(function () {
        var networkGraph = NetworkGraph(dateNow, data.network, timeScale)
        var wrapper = document.getElementById('networkGraphWrapper')
        wrapper.appendChild(networkGraph.element)
    })()

    ;(function () {
        var temperatureGraph = TemperatureGraph(dateNow, data.temperature, timeScale)
        var wrapper = document.getElementById('temperatureGraphWrapper')
        wrapper.appendChild(temperatureGraph.element)
    })()

    ;(function () {
        var storageGraph = StorageGraph(dateNow, data.storage.values, timeScale)
        var wrapper = document.getElementById('storageGraphWrapper')
        wrapper.appendChild(storageGraph.element)
    })()

    ;(function () {
        var fileSystems = document.getElementById('fileSystems')
        if (fileSystems) {
            var graphs = fileSystems.querySelectorAll('.graph')
            for (var i = 0; i < graphs.length; i++) {
                var values = data.storage.fileSystems[i].values
                var storageGraph = StorageGraph(dateNow, values, timeScale)
                graphs[i].appendChild(storageGraph.element)
            }
        }
    })()

})(data, timeScale)
;

})()