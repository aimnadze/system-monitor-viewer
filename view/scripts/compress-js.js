#!/usr/bin/env node

process.chdir(__dirname)
process.chdir('..')

var fs = require('fs')

try {
    var uglifyJs = require('uglify-js')
} catch (e) {
    console.log('ERROR: module uglify-js not found. run "npm install uglify-js" to install.')
    process.exit(1)
}

var files = [
    'javascript/FormatBytes.js',
    'javascript/FormatPercent.js',
    'javascript/Graph.js',
    'javascript/Legend.js',
    'javascript/MemoryGraph.js',
    'javascript/NetworkGraph.js',
    'javascript/ProcessorGraph.js',
    'javascript/StorageGraph.js',
    'javascript/TemperatureGraph.js',
    'index.js',
]


var source = '(function () {\n'
files.forEach(function (file) {
    source += fs.readFileSync(file, 'utf8') + ';\n'
})
source += '\n})()'

var ast = uglifyJs.parse(source)
ast.figure_out_scope()
var compressor = uglifyJs.Compressor({})
var compressedAst = ast.transform(compressor)
compressedAst.figure_out_scope()
compressedAst.compute_char_frequency()
compressedAst.mangle_names()
var compressedSource = compressedAst.print_to_string()

fs.writeFileSync('index.combined.js', source)
fs.writeFileSync('index.compressed.js', compressedSource)
