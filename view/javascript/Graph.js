function Graph (lastTime, items, timeScale) {

    var classPrefix = 'Graph'

    items = items.slice()

    var canvas = document.createElement('canvas')
    canvas.className = classPrefix + '-canvas'
    canvas.width = 400
    canvas.height = 100

    var padding = 2,
        graphWidth = canvas.width - padding * 2,
        graphHeight = canvas.height - padding * 2

    var element = document.createElement('div')
    element.className = classPrefix

    if (items.length) {

        function scale (value) {
            if (maxValue == minValue) return -graphHeight / 2
            return -(value - minValue) / (maxValue - minValue) * graphHeight
        }

        var maxValue = -Infinity, minValue = Infinity
        items.forEach(function (item) {
            maxValue = Math.max(maxValue, item.value)
            minValue = Math.min(minValue, item.value)
        })

        var c = canvas.getContext('2d')
        c.lineWidth = 1.4
        c.lineCap = c.lineJoin = 'round'
        c.translate(canvas.width - padding, canvas.height - padding)
        c.beginPath()
        var prevItem = items.pop()
        var prevY = scale(prevItem.value)
        items.reverse()
        c.arc(0, prevY, 1.4, 0, Math.PI * 2)
        c.moveTo(0, prevY)
        items.forEach(function (item) {
            var x = (prevItem.time - item.time) / (1000 * 60) * (graphWidth / timeScale)
            var y = scale(item.value)
            c.translate(-x, 0)
            c.bezierCurveTo(x / 2, prevY, x / 2, y, 0, y)
            prevItem = item
            prevY = y
        })
        c.strokeStyle = '#07f'
        c.stroke()

        element.appendChild(canvas)

    } else {
        var emptyElement = document.createElement('div')
        emptyElement.className = classPrefix + '-empty'
        emptyElement.appendChild(document.createTextNode('No data avaialable'))
        element.appendChild(emptyElement)
    }

    return { element: element }

}
