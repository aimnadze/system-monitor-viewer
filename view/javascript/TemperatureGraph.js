function TemperatureGraph (lastTime, items, timeScale) {

    var graph = Graph(lastTime, items, timeScale)

    if (items.length) {

        var minValue = Infinity,
            maxValue = -Infinity
        items.forEach(function (item) {
            var value = item.value
            if (value > maxValue) maxValue = value
            if (value < minValue) minValue = value
        })

        var legend = Legend()
        legend.addLine(document.createTextNode('Max: ' + maxValue.toFixed(1) + '\xb0c'))
        legend.addLine(document.createTextNode('Min: ' + minValue.toFixed(1) + '\xb0c'))
        graph.element.appendChild(legend.element)

    }

    return graph

}
