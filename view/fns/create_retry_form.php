<?php

function create_retry_form ($address, $hostHeader, $key) {
    $href = '?'.htmlspecialchars(
        http_build_query([
            'address' => $address,
            'hostHeader' => $hostHeader,
            'key' => $key,
        ])
    );
    return
        '<div>'
            ."<a class=\"button\" href=\"$href\">Retry</a>"
        .'</div>';
}
