<?php

function format_uptime ($uptime) {
    $t = floor($uptime / 60);
    $minutes = str_pad(floor($t % 60), 2, '0', STR_PAD_LEFT);
    $t = floor($t / 60);
    $hours = str_pad(floor($t % 24), 2, '0', STR_PAD_LEFT);
    $days = floor($t / 24);
    if ($days) $text = "$days days, $hours:$minutes";
    else $text = "$hours:$minutes";
    return $text;
}
